import './App.css';
import { createTheme ,NextUIProvider } from '@nextui-org/react';
import InputPage from "./pages/InputPage"

function App() {

  const darkTheme = createTheme({
    type: 'dark',
    theme: {
      // colors: {...},
    }
  })

  console.log("Bon anniversaire !")
  return (
    <NextUIProvider theme={darkTheme}>
      <div className="App">
        <InputPage />
      </div>
    </NextUIProvider>
  );
}

export default App;
