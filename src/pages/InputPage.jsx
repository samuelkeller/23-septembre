import React from "react";
import { Button, Input, Spacer, Tooltip } from '@nextui-org/react';
import { useState } from "react";

function InputPage() {
    const [inputValue, setInputValue] = useState("")
    const [errorMessage, setErrorMessage] = useState("")
    const [found, setFound] = useState(false)

    const submitForm = (e) => {
        const a = 45.31
        const b = 4.73
        if (inputValue.includes("45.31, 4.73") ||
            inputValue.includes("45.31") ||
            inputValue.includes("4.73")) {
            setErrorMessage("Ce code doit surement dire quelque chose 🤔")
        } else if (inputValue === (a * b).toString() ||
            inputValue === (a / b).toString() ||
            inputValue === (a - b).toString() ||
            inputValue === (a + b).toString()) {
            setErrorMessage("Pas des maths")
        } else if (inputValue.toLowerCase() === "toulouse" || inputValue.toLowerCase() === "berlioz") {
            setErrorMessage("Parfois avoir des chats ne suffit pas")
        } else if (inputValue.toLowerCase() === "felines") {
            setFound(true);
        }
        else {
            setErrorMessage("Incorrect")
        }
    }

    const clearError = () => {
        setErrorMessage("")
    }

    return (
        <>
            <header className="center-body">
                {
                    found ? <h3 color="success" >
                        En gros: y’a des rouges, des jaunes, des bleurs et des verts et puis on est côté
                        toilettes ou bien forêt. Et en gros on fait rouler le truc carré la et du coup on peut pas mettre les mettre n’importe ou,
                        après c’est un règle difficile donc parfois même si on le fait rouler on met pas partout.
                        Alors après faut mettre les pareils ou les pas pareil, y’a la piramide mais personne comprend trop comment ça marche.
                        Ah oui et il y a la rivière aussi.
                    </h3> :
                        <>
                            <p>
                                Bonsoir <code>GAELLE</code>
                            </p>
                            <Input
                                placeholder="Code secret"
                                size="xl"
                                clearable
                                onClearClick={clearError}
                                value={inputValue} onChange={e => setInputValue(e.target.value)}
                                helperColor="error"
                                helperText={errorMessage}
                            />
                            <Spacer y={2} />
                            <Button bordered color="gradient" onClick={submitForm}>Envoyer</Button>

                            <Tooltip className="no-center" content={"Chacun son problème"}>
                                <span>Je n'ai pas de code secret</span>
                            </Tooltip>
                        </>
                }
            </header>
        </>

    )
}

export default InputPage;